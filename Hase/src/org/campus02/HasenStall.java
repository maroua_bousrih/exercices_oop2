package org.campus02;

import java.util.ArrayList;

public class HasenStall {
	protected ArrayList<Hase> hasenstall=new ArrayList<Hase>();
	
	public void addHase(Hase hase)
	{
		hasenstall.add(hase);
	}
	public void fressen()
	{
		for (Hase hase : hasenstall) 
		{
			hase.fressen();
		}
	}
	
	public void schlaffen()
	{
		for (Hase hase : hasenstall) {
			hase.schlaffen();
		}
	}
	
	
	

}
