package hashmap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;


public class demo {

	public static void main(String[] args) {
		HashMap<String, Integer> bezirkeMap= new HashMap<>();
		System.out.println(bezirkeMap);
		bezirkeMap.put("Graz", 273838);
		bezirkeMap.put("Wien",50000);
		bezirkeMap.put("Deutschlandberg",60410);
		bezirkeMap.put("Graz", 58854441);
		System.out.println(bezirkeMap);
		System.out.println("Schl�ssel vom HashMap");
				
		Set<String> keySet = bezirkeMap.keySet();
		
		
		for(String key:keySet)
		{
			System.out.println("bezirk: " + key);
		}
		System.out.println("Value vom HashMap");
		
		Collection<Integer> einwohner= bezirkeMap.values();
		for(Integer temp : einwohner)
		{
			System.out.println(temp);
		}
		System.out.println("Key and Values from HashMap");
		
	
		Set<Entry<String,Integer>> keyvalues = bezirkeMap.entrySet();
		for(Entry <String, Integer> entry: keyvalues)
		{
			System.out.println("Bezirk:  " + entry.getKey() + " Einwohner: " + entry.getValue());
		}
		// einwohner von wien
		Integer w= bezirkeMap.get("Wien");
		System.out.println("Einwohner von Wien: " + w);
	System.out.println("*******************");
	System.out.println("Remove key wien");
		bezirkeMap.remove("Wien");
		
		for(Entry <String, Integer> entry: keyvalues)
		{
			System.out.println("Bezirk:  " + entry.getKey() + " Einwohner: " + entry.getValue());
		}
		
		bezirkeMap.clear();
		System.out.println(bezirkeMap);
		System.out.println("is leer ?"+ bezirkeMap.isEmpty());
		
		
	}
}
