package org.campus02.animals;

public class DemoAnimals {
	public static void main(String[] args) {
		Dog  dog1 = new Dog();
		dog1.name="Peppi ";
		dog1.makeNoise();
		
		Cat cat1= new Cat();
		cat1.name="Mitzi ";
		cat1.makeNoise();
		cat1.move();
		
		Beagle beagle1 = new Beagle();
		beagle1.name="Beagle";
		beagle1.loveFood="Knochen";
		beagle1.makeNoise();
		beagle1.move();
		
		
	}

}
