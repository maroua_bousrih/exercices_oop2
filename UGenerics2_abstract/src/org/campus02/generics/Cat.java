package org.campus02.generics;

public class Cat extends Animal {
	private String name;
	private double weight;
	private String loveliness;
	public Cat(String name, double weight, String loveliness) {
		super();
		this.name = name;
		this.weight = weight;
		this.loveliness = loveliness;
	}
	@Override
	public String toString() {
		return "Cat [name=" + name + ", weight=" + weight + ", loveliness=" + loveliness + "]";
	}
	 public void makeNoise()
	 {
		 System.out.println(name+ " Miau,Miau");
	 }
	public String getName() {
		return name;
	}
	public double getWeight() {
		return weight;
	}
	public String getLoveliness() {
		return loveliness;
	}

	

}
