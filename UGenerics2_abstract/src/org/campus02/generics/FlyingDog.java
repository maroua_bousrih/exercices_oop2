package org.campus02.generics;

public class FlyingDog extends Dog implements Fly{

	
	public FlyingDog(String name, String breed, double weight) {
		super(name, breed, weight);
	
	}

	@Override
	public void fly() {
		System.out.println("Fliegende Hund");		
	}

}
