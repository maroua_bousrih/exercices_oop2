package org.campus02.generics;

public class TransportBox<T> extends Box{
	private String color;
	private double price;
	private T content;
	
	public TransportBox(String color, int length, int width, int height, double price,T content) {
		super(length,width,height);
		this.color = color;
		this.price = price;
		this.content=content;
	}

	
	@Override
	public String toString() {
		return "TransportBox [color=" + color + ", price=" + price + ", toString()=" + super.toString() + "]";
	}


	public int calcVolume()
	{
		return (int) (this.length*this.length*this.width * 0.7);
				
	}


	public String getColor() {
		return color;
	}


	


	public double getPrice() {
		return price;
	}


	

	
	
	

}
