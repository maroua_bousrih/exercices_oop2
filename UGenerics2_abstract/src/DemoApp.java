import java.util.ArrayList;
import java.util.HashMap;

import org.campus02.generics.Box;
import org.campus02.generics.Cat;
import org.campus02.generics.Dog;
import org.campus02.generics.TransportBox;

public class DemoApp {

	public static void main(String[] args) {
	Dog dog= new Dog("Wuffi","Sch�fer",4000);
	Cat cat = new Cat("schnurli",2500,"besondersliebvoll");
	Box boxdemo=new Box(10,10,10);
	System.out.println("Volumen von Box " + boxdemo.calcVolume());
	
	TransportBox<Dog> transportBox= new TransportBox<Dog>("red",10,10,10,99.12,dog);
	
	System.out.println("Volumen von transportbox "+ transportBox.calcVolume());
	
	TransportBox<Cat> transportBox0= new TransportBox<Cat>("red",10,10,10,99.12,cat);
	TransportBox<Cat> transportBox1= new TransportBox<Cat>("blue",10,10,10,99.12,cat);
	TransportBox<Dog> transportBox2= new TransportBox<Dog>("green",10,10,10,99.12,dog);
	TransportBox<Dog> transportBox3= new TransportBox<Dog>("white",10,10,10,99.12,dog);
	TransportBox<Dog> transportBox4= new TransportBox<Dog>("black",10,10,10,99.12,dog);
	
	ArrayList<TransportBox> listeBoxen=new ArrayList<>();
	
	listeBoxen.add(transportBox);
	listeBoxen.add(transportBox0);
	listeBoxen.add(transportBox1);
	listeBoxen.add(transportBox2);
	listeBoxen.add(transportBox3);
	listeBoxen.add(transportBox4);


	for (TransportBox  tb : listeBoxen) {
		System.out.println(tb.calcVolume());
	}
	
	System.out.println(getCountPerColor(listeBoxen));
	}
//Anzahl der Boxen je Farbe
	public static HashMap<String, Integer> getCountPerColor(ArrayList<TransportBox> listeBoxen)
	{
		HashMap<String,Integer> result= new HashMap<String, Integer>();
		for (TransportBox transportBox : listeBoxen) {
			if(result.containsKey(transportBox.getColor()))
			{
				int count = result.get(transportBox.getColor());
				count++;
				result.put(transportBox.getColor(), count);
			}
			else
			{
				result.put(transportBox.getColor(), 1);
			}
		}
		return result;
		
	}
}
