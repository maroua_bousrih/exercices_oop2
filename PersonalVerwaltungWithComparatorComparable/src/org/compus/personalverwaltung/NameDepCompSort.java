package org.compus.personalverwaltung;

import java.util.Comparator;

public class NameDepCompSort implements Comparator<Employee> {

	@Override
	public int compare(Employee employee1, Employee employee2) {
		int result= employee1.getName().compareTo(employee2.getName());
		if( result == 0)
			result=employee1.getDepartment().compareTo(employee2.getDepartment());
		return result;
	}

	

}
