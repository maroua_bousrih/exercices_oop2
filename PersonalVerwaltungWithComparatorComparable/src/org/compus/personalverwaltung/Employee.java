package org.compus.personalverwaltung;

public class Employee implements Comparable<Employee>{

	private int empNumber;
	private String name,department;
	private double salary;
	public Employee(int empNumber, String name, String department, double salary) {
		super();
		this.empNumber = empNumber;
		this.name = name;
		this.department = department;
		this.salary = salary;
	}
	public int getEmpNumber() {
		return empNumber;
	}
	public String getName() {
		return name;
	}
	public String getDepartment() {
		return department;
	}
	public double getSalary() {
		return salary;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + empNumber;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(salary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (empNumber != other.empNumber)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(salary) != Double.doubleToLongBits(other.salary))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Employee [empNumber=" + empNumber + ", name=" + name + ", department=" + department + ", salary="
				+ salary + "]";
	}
	@Override
	public int compareTo(Employee o) {
		if(empNumber < o.empNumber) 
			return -1;
		if(empNumber > o.empNumber) 
			return 1;
		return 0;
		
		//Absteigen
		//return Integer.compare(empNumber, o.empNumber) * -1;
		//Aufsteigen 
		//return Integer.compare(empNumber, o.empNumber);
	}
		


}
