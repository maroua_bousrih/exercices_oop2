package example;

public class Auto {

	private String name;
	public Auto(String name)
	{
		this.name=name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Auto [name=" + name + "]";
	}
	public void fahren()
	{
		System.out.println("Ich Fahre");
	}
}
