import org.campus02.mitarbeiter.Employee;
import org.campus02.mitarbeiter.EmployeeManager;
import org.campus02.mitarbeiter.FixComissionEmployee;
import org.campus02.mitarbeiter.PercentComissionEmployee;

public class DemoApp {
	public static void main(String[] args) {
		PercentComissionEmployee e1= new PercentComissionEmployee("bousrih", "maroua", "developement", 3500,5);
		FixComissionEmployee e2= new FixComissionEmployee("Taamallah", "bassem", "Firmware", 4000,1000);
		FixComissionEmployee e3= new FixComissionEmployee("M�ller", "Markus", "developement", 3000,350);
		

		EmployeeManager nxp= new EmployeeManager();
		nxp.addEmployee(e1);
		nxp.addEmployee(e2);
		nxp.addEmployee(e3);
		System.out.println(e1);
		System.out.println(e2);
		System.out.println(e3);
		System.out.println(nxp.calcTotalSalary());
		
		System.out.println(nxp.getSalaryByDepartement());
		
		
	}

}
