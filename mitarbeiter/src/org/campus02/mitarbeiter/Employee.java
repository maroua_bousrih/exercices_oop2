package org.campus02.mitarbeiter;

public class Employee {
	protected String lastname, firstname,department;
	protected double baseSalary;
	public Employee(String lastname, String firstname, String department, double baseSalary) {
		super();
		this.lastname = lastname;
		this.firstname = firstname;
		this.department = department;
		this.baseSalary = baseSalary;
	}
	public String getLastname() {
		return lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getDepartment() {
		return department;
	}
	public double getBaseSalary() {
		return baseSalary;
	}
	@Override
	public String toString() {
		return "Employee [lastname=" + lastname + ", firstname=" + firstname + ", department=" + department
				+ ", baseSalary=" + baseSalary + "]";
	}
	
	public double getFullSalary()
	{
		return baseSalary;
	}
	
	

}
