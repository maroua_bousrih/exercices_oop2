package org.campus02.mitarbeiter;

public class FixComissionEmployee extends Employee{

	private double additionalCommission;

	public FixComissionEmployee(String lastname, String firstname, String department, double baseSalary,
			double additionalCommission) {
		super(lastname, firstname, department, baseSalary);
		this.additionalCommission = additionalCommission;
	}
	
	public double getFullSalary()
	{
		return this.baseSalary + this.additionalCommission;
	}

	public double getAdditionalCommission() {
		return additionalCommission;
	}

	@Override
	public String toString() {
		return "FixComissionEmployee [additionalCommission=" + additionalCommission +  super.toString()
				+ "]";
	}
	
	
}
