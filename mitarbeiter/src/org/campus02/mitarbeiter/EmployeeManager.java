package org.campus02.mitarbeiter;

import java.util.ArrayList;
import java.util.HashMap;

public class EmployeeManager {
	private ArrayList<Employee> listEmployee= new ArrayList<>();
	
	public void addEmployee(Employee e)
	{
		listEmployee.add(e);
	}

	public double calcTotalSalary()
	{
		double result=0;
		for (Employee employee : listEmployee) {
		
			result = result + employee.getFullSalary();
			
		}
		return result;
	}
	
	public HashMap<String,Double>getSalaryByDepartement()
	{
		HashMap<String,Double> result= new HashMap<String,Double>();
		
		for (Employee employee : listEmployee) {
			//überprüfen if hashMap hat den Schlüssel
			if(result.containsKey(employee.getDepartment()))
			{
				//auslesen, bearbeiten,ablegen
				
				double sumSalary =  result.get(employee.getDepartment()); //auslesen
				sumSalary = sumSalary + employee.getFullSalary(); // bearbeiten
				result.put(employee.getDepartment(), sumSalary);  //ablegen
			}
			else
			{
				//departement noch nicht in HashMap
				result.put(employee.getDepartment(), employee.getFullSalary());
						
			}
		}
		
		return result;
		
	}
}
