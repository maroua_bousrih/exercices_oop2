package org.campus02.mitarbeiter;

public class PercentComissionEmployee extends Employee {
	private double percentCommision;

	public PercentComissionEmployee(String lastname, String firstname, String department, double baseSalary,
			double percentCommision) {
		super(lastname, firstname, department, baseSalary);
		this.percentCommision = percentCommision;
	}

	
	public double getPercentCommision() {
		return percentCommision;
	}


	public double getFullSalary()
	{
		return this.baseSalary + this.baseSalary* percentCommision /100;
	}


	@Override
	public String toString() {
		return "PercentComissionEmployee [percentCommision=" + percentCommision + super.toString()
				+ "]";
	}
	

}
