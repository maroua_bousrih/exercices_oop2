package org.campus02.ue.emp;
import java.util.ArrayList;

public class EmployeeManager 
{
	private ArrayList<Employee> employees = new ArrayList<Employee>();
	public void addEmployee(Employee e)
	{
		employees.add(e);
	}
	public Employee findByEmpNumber(int number) 
	{
		for (Employee e : employees)
		{
			if (e.getEmpNumber() == number) 
				return e;	
		}
		return null;
	}
	public ArrayList<Employee> findByDepartment(String department) 
	{
		ArrayList<Employee> found = new ArrayList<>();
		for (Employee employee : employees) 
		{
			if(employee.getDepartment().equalsIgnoreCase(department))
				found.add(employee);
		}
		return found;
	}
	public Employee findByMaxSalary() 
	{
		double max = 0.0;
		Employee WithMaxSalary = null;
		for (Employee employee : employees)
		{
			if(employee.getSalary() < max)
			{
				max= employee.getSalary();
				WithMaxSalary = employee;
			}
		}	
		return WithMaxSalary;
	}
	public ArrayList<Employee> getAllEmployees() 
	{
		return this.employees;
	}
}
