package org.campus02.ue.emp;

public class DemoOrgMedium {

	public static void main(String[] args) {
		
		EmployeeManager em = new EmployeeManager();
		
		//create employees and build hierarchy
		//according to organigram
		Employee ceo = new Employee(1, "Employee Name1", 7_500, "Management", "CEO");
		
		Employee cto = new Employee(2, "Employee Name2", 6_500, "Management", "CTO");
		cto.setSuperior(ceo);
		ceo.addSubordinate(cto);
		
		Employee dev1 = new Employee(5, "Employee Name5", 3_500, "Engineering", "Developer");
		dev1.setSuperior(cto);
		Employee dev2 = new Employee(6, "Employee Name6", 3_500, "Engineering", "Developer");
		dev2.setSuperior(cto);
		Employee dev3 = new Employee(7, "Employee Name7", 3_500, "Engineering", "Developer");
		dev3.setSuperior(cto);
		
		cto.addSubordinate(dev1);
		cto.addSubordinate(dev2);
		cto.addSubordinate(dev3);
		
		Employee cio = new Employee(3, "Employee Name3", 6_500, "Management", "CIO");
		cio.setSuperior(ceo);
		ceo.addSubordinate(cio);
		
		Employee cfo = new Employee(4, "Employee Name4", 6_500, "Management", "CFO");
		cfo.setSuperior(ceo);
		ceo.addSubordinate(cfo);

		Employee con1 = new Employee(8, "Employee Name8", 2_500, "Engineering", "Controller");
		con1.setSuperior(cfo);		
		
		cfo.addSubordinate(con1);
		
		//register all employees
		em.addEmployee(ceo);
		em.addEmployee(cto);
		em.addEmployee(cio);
		em.addEmployee(cfo);
		em.addEmployee(dev1);
		em.addEmployee(dev2);
		em.addEmployee(dev3);
		em.addEmployee(con1);
		
	
		//walking down the organigram without indent
		String result = OrganigramHandler.processHierarchy(ceo);
		System.out.println(result);
		
		//walking down the organigram with indent
		result = OrganigramHandler.processHierarchy(ceo,"");
		System.out.println(result);
		
		Double result2= OrganigramHandler.getTotalSalary(ceo);
		System.out.println(result2);
			
	}

}
