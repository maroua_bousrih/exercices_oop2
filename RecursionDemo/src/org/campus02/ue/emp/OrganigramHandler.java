package org.campus02.ue.emp;
public class OrganigramHandler {
	public static String processHierarchy(Employee emp) {
		//emp.getSubordinates();//liefert liste von Mitarbeitern 
		String result= emp.getName()+"\n";
		//Sich selbst ausgeben 
		//System.out.println(emp);
		//alle mitarbeiter durchlaufen
			//jedem Mitarabeiter zui sagen, er soll sich selbst ausgeben
			// und wiederrum jedem seiner Mitarbeiter sagen, dass sie selbsr ausgeben
			//und weiter......
		for(Employee e: emp.getSubordinates())
		{
			result += processHierarchy(e);
		}
		return result;
	}
	public static String processHierarchy(Employee emp, String indent) {
		//result:
			// ceo
				// mAX mUSTER
					// susi
					//john
				//Xaver
					//Alfred
					//Michi
		String result=indent+" > "+emp.getName()+"\n";
		for(Employee e: emp.getSubordinates())
		{
			result += processHierarchy(e, indent +"  ");
		}
		return result;
	}
	public static double getTotalSalary(Employee emp)
	{
		double salary= emp.getSalary();
		for (Employee e: emp.getSubordinates()) {
			salary += getTotalSalary(e);
		}
		return salary;
	}
}
