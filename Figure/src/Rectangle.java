
public class Rectangle  implements Figure{
	private double length, width;
	public Rectangle(double length, double width)
	{
		super();
		this.length=length;
		this.width=width;
	}
	@Override
	public double getPerimeter() 
	{
		return 2*(this.length + this.width);
	}

	@Override
	public double getArea() 
	{
		return length * width;
	}

}
