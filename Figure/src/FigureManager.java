import java.util.ArrayList;
import java.util.HashMap;

public class FigureManager {
	private ArrayList<Figure> figures= new ArrayList<>();	
	public void add(Figure  figure)
	{
		figures.add(figure);
	}
	public double getMaxPerimeter()
	{
		double max=0;
		for (Figure figure : figures) {
			if(figure.getPerimeter()>max)
				max=figure.getPerimeter();
		}
		return max;
	}
	
	public double getAverageAreaSize()
	{
		double sum=0;
		for (Figure figure : figures) {
			sum += figure.getArea();
		}
		return sum/figures.size();
	}
//	public HashMap<String,double> getAreaBySizeCategories()
//	{
//		HashMap<String,Double> erg= new HashMap<>();
//		
//	}

}
