package org.campus02.generics;

public class Box {
	protected int length;
	protected int width;
	protected int height;
	public Box(int length, int width, int height) {
		
		this.length = length;
		this.width = width;
		this.height = height;
	}

	public int calcVolume()
	{
		return length * height * width;
		
	}

	@Override
	public String toString() {
		return "Box [length=" + length + ", width=" + width + ", height=" + height + "]";
	}
	
	

}
