import org.campus02.generics.Animal;
import org.campus02.generics.Cat;
import org.campus02.generics.Dog;

public class DemoAppCasting {

	public static void main(String[] args) {
		Dog dog= new Dog("Wuffi","Sch�fer",4000);
		Dog bello = new Dog("bello","Dackel",3500);
		Cat cat = new Cat("schnurli",2500,"besondersliebvoll");
		
		dog.makeNoise();
		bello.makeNoise();
		cat.makeNoise();
		//Up casting
		System.out.println("***********Up casting***********");
		Animal a = dog;
		a.makeNoise();
		System.out.println("*********and Down*********");
		
		//down Casting
		Dog xyz=(Dog) a;
		
		System.out.println(xyz.getName());
		System.out.println("Erkl�rung down cast, wieso hin und wieder sinnvoll");
		System.out.println("*****************");
		calculateShippingCost(a);
		System.out.println("***********Vertikals casting***********");
		int x =10;
		int y=4;
		int erg = (x/y);
		System.out.println(x/(double) y);
	}
	
	public static void calculateShippingCost(Animal xyz)
	{
		xyz.makeNoise();
		System.out.println(xyz.getClass());
		if(xyz instanceof Dog)
		{
			Dog newDog= (Dog) xyz;
			System.out.println("Das Gewicht von Dog ist: " +newDog.getWeight());
		}
		else if(xyz instanceof Cat)
		{
			Cat newCat= (Cat) xyz;
			System.out.println("Das Gewicht von cat ist: " +newCat.getWeight());
		}
	}
}
