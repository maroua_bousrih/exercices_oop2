
public class Demo {


	public static void main(String[] args) {
	
		System.out.println("Fibonacci von 8 ist "+FibonacciRekursion(8));
		System.out.println(pallindron("rentner"));
		System.out.println(pallindron("ErikafeuertnuruntreueFakire"));

		
		//System.out.println(pallindron("rentnscser"));
	}
	public static int FibonacciRekursion(int n)
	{
		//1- Abrr
		if(n <= 2)
			return 1;

		//1:1
		//2:1
		//3:(n-1)+(n-2) | 1+1 =2
		//4:(n-1)+(n-2) | 2+1 = 3
		//5:(n-1)+(n-2) | 3+2 = 5
		return  FibonacciRekursion(n-1)+FibonacciRekursion(n-2);
		
	}
	
	public static boolean pallindron(String text)
	{	
		if(text.length() <= 1)
			return true;
		
		if(text.toLowerCase().charAt(0) != text.toLowerCase().charAt(text.length()-1))
			return false;
		
		String textkurz = text.substring(1, text.length()-1);
		return pallindron(textkurz);
	}

}
