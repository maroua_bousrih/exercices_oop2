package org.camous02.sort.array;

import java.util.Comparator;

public class AgeSortieren implements Comparator<Person>{

	public AgeSortieren() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compare(Person o1, Person o2) {
		if(o1.getAge() < o2.getAge()) return -1;
		if(o2.getAge()>o2.getAge()) return 1;
		return 0;
	}

}
