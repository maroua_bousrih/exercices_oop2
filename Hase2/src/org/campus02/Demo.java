package org.campus02;

public class Demo {
	public static void main(String[] args) {
		
		Hase hase1= new Hase("coucou");
		hase1.hoppeln();
		hase1.fressen();
		hase1.schlaffen();
		System.out.println("----------------");

		weihnachtshase wh= new weihnachtshase("mimi");
		wh.vertteiltGeschenke();
		wh.fressen();
		System.out.println("----------------");
		OsterHase oh=new OsterHase("lili");
		oh.OstereierVerstecken();
		oh.fressen();
		oh.schlaffen();
		System.out.println("----------");
		HasenStall list= new HasenStall();
		list.addHase(hase1);
		list.addHase(wh);
		list.addHase(oh);
		list.fressen();
		list.schlaffen();
		System.out.println("----------------");
		System.out.println("Up-casting");
		Hase xx= new  weihnachtshase("ffff");
		Hase yy= new  OsterHase("yyyyy");
		yy.fressen();
		
		System.out.println("down casting");
		OsterHase neuerOsterhase = (OsterHase) yy;
		neuerOsterhase.OstereierVerstecken();
		
		
		System.out.println("8888888888");
		list.zeigeHasen();
	}

	
	
}
