package org.campus02;

import java.util.ArrayList;
import java.util.List;

public class HasenStall {
	protected List<Hase> hasenstall=new ArrayList<Hase>();
	
	public void addHase(Hase hase)
	{
		hasenstall.add(hase);
	}
	public void fressen()
	{
		for (Hase hase : hasenstall) 
		{
			hase.fressen();
		}
	}
	
	public void schlaffen()
	{
		for (Hase hase : hasenstall) {
			hase.schlaffen();
		}
	}
	public void setHasen(List<Hase> list)
	{
		this.hasenstall=list;
	}
	
	public void zeigeHasen()
	{
		for (Hase hase : hasenstall) {
			if(hase instanceof OsterHase )
			{
				OsterHase oh= (OsterHase)hase;
				oh.OstereierVerstecken();
			}
			if(hase instanceof weihnachtshase )
			{
				weihnachtshase wh=(weihnachtshase)hase;
				wh.vertteiltGeschenke();
			}

			
		}
	}
	
	
	

}
