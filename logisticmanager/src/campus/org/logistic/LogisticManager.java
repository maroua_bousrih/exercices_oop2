package campus.org.logistic;

import java.util.ArrayList;

public class LogisticManager {
	private ArrayList<Moveable> list= new ArrayList<>();
	
	public void Add(Moveable x)
	{
		list.add(x);
	}
	
	public void moveAll(String destination)
	{
		for (Moveable moveable : list) {
			moveable.move(destination);
			
		}
	
	}

	@Override
	public String toString() {
		return "LogisticManager [list=" + list + ", toString()=" + super.toString() + "]";
	}
	
	

}
