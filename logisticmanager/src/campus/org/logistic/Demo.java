package campus.org.logistic;

public class Demo {

	public static void main(String[] args) {

		LogisticManager knapp= new LogisticManager();
		
		Car car1= new Car("BMW","red",200000);
		Car car2= new Car("Opel","blue",202300);
		Car car3= new Car("VW","black",200000);
		Shirt shirt1= new Shirt("xddd", "black", "M");
		Shirt shirt2= new Shirt("sjsss", "white", "L");
		knapp.Add(car1);
		knapp.Add(shirt1);
		knapp.Add(car2);
		knapp.Add(car3);
		knapp.Add(shirt2);
		
		knapp.moveAll("Tunesien");
		
		knapp.moveAll("Wien");

		System.out.println(knapp);

	}

}
