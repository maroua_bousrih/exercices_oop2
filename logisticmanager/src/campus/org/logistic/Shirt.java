package campus.org.logistic;

public class Shirt implements Moveable{
	private String brand, color,size;

	public Shirt(String brand, String color, String size) {
		super();
		this.brand = brand;
		this.color = color;
		this.size = size;
	}

	public String getBrand() {
		return brand;
	}

	public String getColor() {
		return color;
	}

	public String getSize() {
		return size;
	}

	@Override
	public String toString() {
		return "Shirt [brand=" + brand + ", color=" + color + ", size=" + size + "]";
	}

	@Override
	public void move(String destination) {
		System.out.println(color +" " + brand + " " + size+  " will be moved to "+ destination );
		
	}
	

}
