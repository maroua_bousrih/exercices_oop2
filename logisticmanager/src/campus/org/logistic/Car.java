package campus.org.logistic;

public class Car implements Moveable{
	 private String type,color;
	 private int weight;
	public Car(String type, String color, int weight) {
		super();
		this.type = type;
		this.color = color;
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "Car [type=" + type + ", color=" + color + ", weight=" + weight + "]";
	}
	public String getType() {
		return type;
	}
	public String getColor() {
		return color;
	}
	public int getWeight() {
		return weight;
	}
	@Override
	public void move(String destination) {
		System.out.println(color +" " + type+ " will be moved to "+ destination );
		
	}
	 
	 

}
